import { NgModule } from '@angular/core';

import { LayoutModule } from '@angular/cdk/layout';
import { FlexLayoutModule } from "@angular/flex-layout";

import {MatCardModule} from '@angular/material/card';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatButtonModule} from '@angular/material/button';
import {MatListModule} from '@angular/material/list';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatChipsModule} from '@angular/material/chips';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatIconModule} from '@angular/material/icon';
import {MatDividerModule} from '@angular/material/divider';
import {MatBadgeModule} from '@angular/material/badge';

import {MatRippleModule} from '@angular/material';

@NgModule ({
  imports: [MatCardModule, MatGridListModule,
    MatToolbarModule, MatChipsModule,
    MatExpansionModule, MatIconModule,
    LayoutModule, FlexLayoutModule,
    MatButtonModule, MatListModule,
    MatSidenavModule, MatRippleModule,
    MatFormFieldModule, MatInputModule,
    MatProgressBarModule, MatBadgeModule],
  exports: [MatCardModule, MatGridListModule,
    MatToolbarModule, MatChipsModule,
    MatExpansionModule, MatIconModule,
    LayoutModule, FlexLayoutModule,
    MatButtonModule, MatListModule,
    MatSidenavModule, MatRippleModule,
    MatFormFieldModule, MatInputModule,
    MatProgressBarModule, MatBadgeModule]
})

export class MovieMaterialModule {}
