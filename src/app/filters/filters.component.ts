import { Component, OnInit } from '@angular/core';

import { FilterService } from '../services/filter.service';

@Component({
  selector: 'app-filters',
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.scss']
})
export class FiltersComponent implements OnInit {
  filters: any;

  constructor(private filterService: FilterService) { 
    this.filters = this.filterService.getFiltersData();
  }

  ngOnInit() {    
    this.filterService.getFilters$()
      .subscribe(filters => {        
        this.filters = filters;
      });
  }

  onSelectGenre(index: number): void {    
    this.filterService.addGenre(index);    
  }

  onRemoveGenre(index: number): void {
    this.filterService.removeGenre(index);
  }

}
