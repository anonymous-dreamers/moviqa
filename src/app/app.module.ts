import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';

import { MovieMaterialModule } from './materials/movie-material.module';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';

import { MoviesListComponent } from './movies-list/movies-list.component';
import { MovieDetailComponent } from './movie-detail/movie-detail.component';
import { FiltersComponent } from './filters/filters.component';
import { GenrePipe } from './pipes/genre.pipe';
import { HeaderComponent } from './header/header.component';
import { SearchComponent } from './header/search/search.component';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import { FooterComponent } from './footer/footer.component';

@NgModule({
  declarations: [
    AppComponent,   
    MoviesListComponent,
    MovieDetailComponent,
    FiltersComponent,
    GenrePipe,
    HeaderComponent,
    SearchComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule, BrowserAnimationsModule,
    ReactiveFormsModule,
    MovieMaterialModule,
    AppRoutingModule,
  ],
  providers: [{provide: LocationStrategy, useClass: HashLocationStrategy}],
  bootstrap: [AppComponent]
})
export class AppModule { }
