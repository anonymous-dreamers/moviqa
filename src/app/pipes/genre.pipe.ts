import { Pipe, PipeTransform } from '@angular/core';

import { Movie } from '../mock-data/movie.model';

@Pipe({
  name: 'genre'
})
export class GenrePipe implements PipeTransform {
  
  transform(value: any, args?: any): Movie[] {   
    if (args.length < 1) {
      return value;
    }

    const result = value.filter(item => {
      return item.genres.some(genre => {
        return args.some(filter => filter === genre);
      });
    });        
    
    return result;
  }
}
