import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Location } from '@angular/common';
import { Movie } from '../mock-data/movie.model';


import { MovieService } from '../services/movie.service';
import {Observable, Subscription, of} from 'rxjs';

@Component({
  selector: 'app-movie-detail',
  templateUrl: './movie-detail.component.html',
  styleUrls: ['./movie-detail.component.scss']
})

export class MovieDetailComponent implements OnInit {
  id: number;
  movie: Movie;
  movie$: Subscription;
  constructor(private movieService: MovieService, private route: ActivatedRoute, private router: Router, private location: Location) { }

  ngOnInit() {    
    this.id = +this.route.snapshot.params['id'];
    this.movie$ = this.movieService.getMovie(this.id).subscribe(movie => {
        this.movie = movie;
    });
  }

  onBack(): void{
    this.location.back();
  }
  ngOnDestroy() {
    this.movie$.unsubscribe();
  }
}
