import { Injectable } from '@angular/core';

import { genreType, GenreType } from '../mock-data/genre.model';
import { Subject, of} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FilterService {
  genres: string[];
  genresFilter: string[];

  filters$: Subject<any>;
  constructor() {
    this.genres = new Array();
    this.genresFilter = new Array();
    this.filters$ = new Subject();
    for (let name in genreType) {
      this.genres.push(name);
    }   
  }

  addGenre(index: number) {
    let element: any = this.genres.splice(index, 1).pop();
    this.genresFilter.push(element);

    this.filters$.next({genres: [...this.genres], genresFilter: [...this.genresFilter]});
  }

  removeGenre(index: number) {
    let element: any = this.genresFilter.splice(index, 1).pop();
    this.genres.push(element);

    this.filters$.next({genres: [...this.genres], genresFilter: [...this.genresFilter]});
  } 

  getFilters$(): Subject<any> {
    return this.filters$;
  }
  
  getFiltersData(): any {
    return {genres: [...this.genres], genresFilter: [...this.genresFilter]};
  }
}
