import { Injectable } from '@angular/core';
import { movies } from '../mock-data/movie.mock-data';
import { Movie } from '../mock-data/movie.model';

import {Observable, of} from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class MovieService {

  constructor() { }

  getMovie(id: number): Observable<Movie> {
    const movie = movies.find(movie => movie.id === id);
    return of(movie);
  }

  getMovies(): Observable<Movie[]> {
    return of(movies);
  }

  findMovies(querys): Observable<Movie[]> {
    return of(querys).pipe(
      map(querys => movies.filter(movie => {
        if (querys[0].length < 1) {
          return movies;
        } else {
          const MOVIE_KEYS = movie.key.split('-');
         //The in the key array is one of the querys element
         return !!MOVIE_KEYS.find(key => querys.find(query => key.indexOf(query) !== -1));
        }
      }))
    );
  }
}
