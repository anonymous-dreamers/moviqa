import { Injectable } from '@angular/core';
import { Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SearchService {  
  searchQuery$: Subject<string[]>;

  constructor() { 
    this.searchQuery$ = new Subject();
  }

  startSearch(querys: string[]): void {
    this.searchQuery$.next(querys);
  }

  getSearchQuery$(): Subject<string[]> {
    return this.searchQuery$;
  }
}
