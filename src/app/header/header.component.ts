import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';

import { Subscription } from 'rxjs';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  searchIsEnabled: boolean = false;
  route$ : Subscription;
  constructor(private route: Router) { }

  ngOnInit() {
    this.route$ = this.route.events.pipe(
      filter((event: any) => event instanceof NavigationEnd)
    ).subscribe(route => {
       this.searchIsEnabled = route.urlAfterRedirects === "/movies";
    });
  }

  ngOnDestroy() {
    this.route$.unsubscribe();
  }

}
