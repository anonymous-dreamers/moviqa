import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormControl } from '@angular/forms';

import { MovieService } from '../../services/movie.service';
import { SearchService } from '../../services/search.service';

import { Subscription } from 'rxjs';
import { map, debounceTime, distinctUntilChanged, pairwise, filter, startWith } from 'rxjs/operators';


@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit, OnDestroy {
  queryFieldValue: string = "";
  queryField: FormControl = new FormControl(); 
  query$: Subscription;

  constructor(private searchService: SearchService) { }

  ngOnInit() {    
      this.query$ = this.queryField.valueChanges.pipe(
      //Start with empty string, for first input
      startWith(''),
      //Get value after 400ms when user stop writing
      debounceTime(400),
      distinctUntilChanged(),
      pairwise(),
      //Pass forward search query only if prevoious and current is not empty string
          filter(pairValues =>                 pairValues[0].concat(pairValues[1]).trim().length > 0),
      map(query => query[1].trim().toLowerCase().split(' ')))      
     .subscribe(querys => this.searchService.startSearch(querys));
  }

  ngOnDestroy() {
    this.query$.unsubscribe();
  }
}
