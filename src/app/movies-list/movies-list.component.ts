import { Component, OnInit, OnDestroy } from '@angular/core';

import { FilterService } from '../services/filter.service';
import { MovieService } from '../services/movie.service';
import { SearchService } from '../services/search.service';

import { Movie } from '../mock-data/movie.model';

import { Subscription } from 'rxjs';
import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-movies-list',
  templateUrl: './movies-list.component.html',
  styleUrls: ['./movies-list.component.scss']
})
export class MoviesListComponent implements OnInit, OnDestroy {
  movies: Movie[];
  genresFilter: string[];
  filters$: Subscription;
  search$: Subscription;

  constructor( private movieService: MovieService, 
    private filterService: FilterService,
    private searchService: SearchService) {
    this.genresFilter = new Array();
  }

  getMovies(): void {
      this.movieService.getMovies()
        .subscribe(movies => this.movies = movies);
  }

  getFilters(): void {
    const filters = this.filterService.getFiltersData();
    this.genresFilter = filters.genresFilter;
  }

  ngOnInit() {
    this.getMovies();
    this.getFilters();

    //Observable for genres filter
    this.filters$ = this.filterService.getFilters$()
      .subscribe(filters => {        
        this.genresFilter = filters.genresFilter;
      });

    
    //Observable for search result films
    this.search$ = this.searchService.getSearchQuery$()
      .pipe(
        switchMap(querys => this.movieService.findMovies(querys)))
      .subscribe(movies => this.movies = movies);
  }

  ngOnDestroy() {
    this.filters$.unsubscribe();
    this.search$.unsubscribe();
  }
  
}
