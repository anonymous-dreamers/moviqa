**MOVIQA - The movies collection app**

MOVIQA is the application, which allow you to search your favorit movies. Also you can filter movies by genres.

Technology stack: Angular 6, TypeScript, Angular Material, RxJS, SASS

LIVE: https://clever-bell-1c74c1.netlify.com/
---

## Search Movies / Movies-List

For finding your favorite movies in the collection, do the following steps:

1. Be sure that you are on the main page. For this click on MOVIQA logo in the toolbar.
2. In search field in toolbar type the title of movie or words which presents in the title .

---

## Filtering movies / Movies-List

You can find movies by genres which you like

1. Click on the Advanced Filters panels.
2. From the "By genres:" row, select genre which you like by click on it.
3. The searching process will start automaticaly.
4. For removing from filtering some genres, click on the Advanced Filters panels, and from "Selected Filters" row, remove genre by click on it.

---

## Movie Detail

From the movies list, you can see the movie details.

1. Click on the movie  which you like, and you will automaticaly load movie details.

---

## Build on localhost

Clone repository on your PC, you should have NPM installed. 

1. Go to folder with project, and run in terminal: npm install
2. After all packages will be install, run in terminal: ng serve
3. When build was done, open your browser and type in adress bar: localhost:4200  (if you optionaly set the port, type it instead 4200)
